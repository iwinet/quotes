<?php

class Quotes
{

    public $quotes = array(

        /* quotes */
        array(
            'quote' => "If you accept this fact (that the choices you make today will most certainly be wrong in the future) then it relieves you of the burden of trying to future-proof your architectures.",
            'author' => "Richard Monson-Haefe",
        ),
        array(
            'quote' => "When you try to guess at future requirements, 50% of the time you’re wrong and 49% of the time you’re very, very wrong.",
            'author' => "Chad LaVigne",
        ),
        array(
            'quote' => "Walking on water and developing software from a specification are easy if both are frozen.",
            'author' => "Edward V Berard",
        ),
        array(
            'quote' => "I have always wished for my computer to be as easy to use as my telephone; my wish has come true because I can no longer figure out how to use my telephone.",
            'author' => "Bjarne Stroustrup",
        ),
        array(
            'quote' => "Computer science education cannot make anybody an expert programmer any more than studying brushes and pigment can make somebody an expert painter.",
            'author' => "Eric S. Raymond",
        ),
        array(
            'quote' => "Good design adds value faster than it adds cost.",
            'author' => "Thomas C. Gale",
        ),
        array(
            'quote' => "Most good programmers do programming not because they expect to get paid or get adulation by the public, but because it is fun to program.",
            'author' => "Linus Torvalds",
        ),
        array(
            'quote' => "Programming today is a race between software engineers striving to build bigger and better idiot-proof programs, and the universe trying to produce bigger and better idiots. So far, the universe is winning.",
            'author' => "Rich Cook",
        ),
        array(
            'quote' => "Programming is like sex. One mistake and you have to support it for the rest of your life.",
            'author' => "Michael Sinz",
        ),
        array(
            'quote' => "There are two ways to write error-free programs; only the third one works.",
            'author' => "Alan J. Perlis",
        ),
        array(
            'quote' => "Don’t worry if it doesn’t work right. If everything did, you’d be out of a job.",
            'author' => "Mosher’s Law of Software Engineering",
        ),
        array(
            'quote' => "Most of you are familiar with the virtues of a programmer. There are three of course: laziness, impatience, and hubris.",
            'author' => "Larry Wall",
        ),
        array(
            'quote' => "Simplicity is prerequisite for reliability",
            'author' => "Edsger W.Dijkstra",
        ),
        array(
            'quote' => "Make everything as simple as possible, but not simpler.",
            'author' => "Albert Einstein",
        ),
        array(
            'quote' => "The two most common elements in the universe are hydrogen and stupidity.",
            'author' => "Harlan Ellison",
        ),
        array(
            'quote' => "Software sucks because users demand it to.",
            'author' => "Nathan Myhrvold ",
        ),
        array(
            'quote' => "Computers are good at following instructions, but not at reading your mind.",
            'author' => "Donald Knuth ",
        ),
        array(
            'quote' => "Measuring programming progress by lines of code is like measuring aircraft building progress by weight.",
            'author' => "Bill Gates",
        ),
        array(
            'quote' => "A computer would deserve to be called intelligent if it could deceive a human into believing that it was human",
            'author' => "Alan Turing",
        ),
        array(
            'quote' => "Je moet gewoon niet te diep nadenken. Dan klopt alles.",
            'author' => "Herman Finkers",
        ),
        array(
            'quote' => "Soms denk ik uren na en heb ik nog niks op papier, een andere keer bereik ik precies datzelfde in vijf minuten.",
            'author' => "Herman Finkers",
        ),
        array(
            'quote' => "Ja taalwetenschappelijk gezien luistert het akelig nauw, waar de leestekens komen te staan. Want één verkeerde komma of letter maakt zelf van Jezus nog een ketter.",
            'author' => "Herman Finkers",
        ),
        array(
            'quote' => "Je kunt vrouwen in twee groepen verdelen… maar ik zou het niet doen.",
            'author' => "Herman Finkers",
        ),
        array(
            'quote' => "Niets zo charmant, als een spreuk aan de wand.",
            'author' => "Herman Finkers",
        ),
        array(
            'quote' => "One in a million is next Tuesday.",
            'author' => "Gordon Letwin",
        ),
        array(
            'quote' => "Software and cathedrals are much the same – first we build them, then we pray.",
            'author' => "Sam Redwine ",
        ),
        array(
            'quote' => "Real Programmers always confuse Christmas and Halloween because Oct31 == Dec25.",
            'author' => "Andrew Rutherford",
        ),
        array(
            'quote' => "Asking for efficiency and adaptability in the same program is like asking for a beautiful and modest wife. Although beauty and modesty have been known to occur in the same woman, we'll probably have to settle for one or the other. At least that's better than neither.",
            'author' => "Gerald M. Weinberg",
        ),
        array(
            'quote' => "If debugging is the process of removing bugs, then programming must be the process of putting them in.",
            'author' => "Edsger W. Dijkstra",
        ),
        array(
            'quote' => "Testing can only prove the presence of bugs, not their absence.",
            'author' => "Edsger W. Dijkstra",
        ),
        array(
            'quote' => "Programmeren is makkelijker als je denkt.",
            'author' => "Edsger W. Dijkstra",
        ),
        array(
            'quote' => "From then on, when anything went wrong with a computer, we said it had bugs in it.",
            'author' => "RADM Grace Hopper, on the removal of a 2-inch-long moth from the Harvard Mark I experimental computer at Harvard in August 1945, as quoted in Time (16 April 1984)",
        ),
        array(
            'quote' => "Elegance is not a dispensable luxury but a factor that decides between success and failure.",
            'author' => "Edsger Dijkstra",
        ),
        array(
            'quote' => "The Internet? We are not interested in it.",
            'author' => "Bill Gates",
        ),
        array(
            'quote' => "I love deadlines. I love the whooshing noise they make as they go by.",
            'author' => "Douglas Adams, The Salmon of Doubt",
        ),
        array(
            'quote' => "The story so far:
    In the beginning the Universe was created.
    This has made a lot of people very angry and been widely regarded as a bad move.",
            'author' => "Douglas Adams, The Restaurant at the End of the Universe",
        ),
        array(
            'quote' => "I refuse to answer that question on the grounds that I don't know the answer",
            'author' => "Douglas Adams",
        ),
        array(
            'quote' => "A common mistake that people make when trying to design something completely foolproof is to underestimate the ingenuity of complete fools.",
            'author' => "Douglas Adams, Mostly Harmless",
        ),
        array(
            'quote' => "The ships hung in the sky in much the same way that bricks don't.",
            'author' => "Douglas Adams, The Hitchhiker's Guide to the Galaxy",
        ),
        array(
            'quote' => "Nothing travels faster than the speed of light, with the possible exception of bad news, which obeys its own special laws.",
            'author' => "Douglas Adams, Mostly Harmless",
        ),
        array(
            'quote' => "Anything that can go wrong, will go wrong",
            'author' => "Murphy's Law",
        ),
        array(
            'quote' => "It always takes longer than you expect, even when you take into account Hofstadter's Law",
            'author' => "Douglas Hofstadter",
        ),
        array(
            'quote' => "Now we need to make sure those simulations and reality agree because generally, when they don't, reality wins.",
            'author' => "Elon Musk, SpaceX, 2011",
        ),
        array(
            'quote' =>"..those people back then shaped the entire world by translating the analog environments they found themselves in into digital, by essentially tricking a little bit of sand into remembering numbers.",
            'author' => "Bruno Skvorc",
        ),
        array(
            'quote' => "If you haven't automatically destroyed something by mistake, you are not automating enough",
            'author' => 'Carlos Sanchez (CloudBees)',
        ),
        array(
            'quote' => "If you are not embarrassed by the first version of your product, you've launched too late.",
            'author' => "Reid Hoffman (LinkedIn)",
        ),

        /* Definitions */
        array(
            'quote' => "Programmer: an organism that turns coffee into software.",
        ),
        array(
            'quote' => "Project Manager: a person who thinks nine women can deliver a baby in one month.",
        ),
        array(
            'quote' => "Algorithm: word used by programmers when they do not want to explain what they did.",
        ),
        array(
            'quote' => "Sarcasm: because beating the shit out of people is illegal.",
        ),

        /* Unknown popular quotes */
        array(
            'quote' => "Have you tried turning it off and on again?",
        ),
        array(
            'quote' => "The best thing about a boolean is even if you are wrong, you are only off by a bit.",
        ),
        array(
            'quote' => "In order to understand recursion, one must first understand recursion.",
        ),
        array(
            'quote' => "Programs for sale: fast, reliable, cheap - choose two.",
        ),
        array(
            'quote' => "There are two difficult problems in computer science:
                - cache invalidation
                - naming things
                - and off by one errors",
        ),
        array(
            'quote' => "The best method for accelerating a computer is the one that boosts it by 9.8 m/s2.",
        ),
        array(
            'quote' => "It’s not a bug - it’s an undocumented feature.",
        ),
        array(
            'quote' => "Yo moma is like HTML: Tiny head, huge body.",
        ),
        array(
            'quote' => "Computers are man's attempt at designing a cat: it does whatever it wants, whenever it wants, and rarely ever at the right time.",
        ),
        array(
            'quote' => "A SQL query walks into a bar. He approaches two tables and says, 'Mind if I join you?'",
        ),
        array(
            'quote' => "WARNING Due to a shortage of robots, workers here are human beings and may act unpredictably if abused. You have been warned.",
        ),
        array(
            'quote' => "In case of fire:
        1. git commit
        2. git push
        3. leave building",
        ),
        array(
            'quote' => "99 little bugs in the code.
        99 little bugs in the code.
        Take one down, patch it around.

        127 little bugs in the code...",
        ),
        array(
            'quote' => "The problem about being a programmer
        My mom said: 'Honey, please go to the market and buy 1 bottle of milk. If they have eggs, bring 6.'
        I came back with 6 bottles of milk.
        She said: 'Why the hell did you buy 6 bottles of milk?'
        I said: 'BECAUSE THEY HAD EGGS!!!!'",
        ),
        array(
            'quote' => "User: Siri, call me an ambulance.
        Siri: Okay, from now on I’ll call you “an ambulance.”",
        ),
        array(
            'quote' => "Hi, would you like to hear a UDP joke?
        - Yes, I would like to hear a UDP joke.
        To get to the other side.
        ...
        Why did the chicken cross the road?",  
        ),


        /* Movie / culture */
        array(
            'quote' => "I am completely operational, and all my circuits are functioning perfectly.",
            'author' => "HAL 9000",
        ),
        array(
            'quote' => "I am putting myself to the fullest possible use. Which is all I think that any conscious entity can ever hope to do.",
            'author' => "HAL 9000",
        ),
        array(
            'quote' => "I'm sorry, Dave. I'm afraid I can't do that.",
            'author' => "HAL 9000"
        ),
        array(
            'quote' => "Wake up, Neo...",
            'author' => "The Matrix"
        ),
        array(
            'quote' => "Follow the white rabbit.",
            'author' => "The Matrix"
        ),
        array(
            'quote' => "SHALL WE PLAY A GAME?",
            'author' => "SAW"
        ),
        array(
            'quote' => "Don't panic",
            'author' => "Cover of 'Hitchhikers Guide To The Galaxy'"
        ),
        array(
            'quote' => "Computer says no",
            'author' => "Carol Beer - Little Britain"
        ),

        /* Comments */
        array(
            'quote' => "//When I wrote this, only God and I understood what I was doing",
        ),
        array(
            'quote' => "//Now, God only knows",
        ),
        array(
            'quote' => "// drunk, fix later",
        ),
        array(
            'quote' => "// Magic. Do not touch.",
        ),
        array(
            'quote' => "/////////////////////////////////////// this is a well commented line",
        ),
        array(
            'quote' => "// I am not sure if we need this, but too scared to delete.",
        ),
        array(
            'quote' => "// I am not responsible for this code.",
        ),
        array(
            'quote' => "// They made me write it, against my will.",
        ),
        array(
            'quote' => "// Dear future me. Please forgive me.",
        ),
        array(
            'quote' => "// I can’t even begin to express how sorry I am.",
        ),
        array(
            'quote' => "// hack for ie browser (assuming that ie is a browser)",
        ),
        array(
            'quote' => "// Catching exceptions is for communists",
        ),

        /* phunny */
        array(
          'quote' => "I bought some shoes from a drug dealer. I don't know what he laced them with, but I've been tripping all day.",
          'author' => "ImHully"
        ),
        array(
        'quote' => "I bought my friend an elephant for his room.
        He said \"Thanks\"
        I said \"Don't mention it\"",
        'author' => "3shirts"
        ),

        array(
        'quote' => "I have an EpiPen. My friend gave it to me when he was dying, it seemed very important to him that I have it.",
        'author' => "kate_winslat"
        ),

        array(
        'quote' => "I poured root beer in a square glass.
        Now I just have beer.",
        'author' => "PM_ME_TINY_DINOSAURS"
        ),

        array(
        'quote' => "What's the difference between a hippo and a zippo?
        One is really heavy, and the other is a little lighter.",
        'author' => "alosercalledsusie"
        ),

        array(
        'quote' => "My friend says to me: \"What rhymes with orange\" I said: \"no it doesn't\"",
        'author' => "DinosRoar1"
        ),

        array(
        'quote' => "And God said to John, come forth and you shall be granted eternal life.
        But John came fifth and won a toaster.",
        'author' => "PM-SOME-TITS"
        ),

        array(
        'quote' => "What do you call a frenchman wearing sandals?
        Phillipe Phillope.",
        'author' => "Sooowhatisthis"
        ),

        array(
        'quote' => "What's orange and sounds like a parrot?
        A carrot.",
        'author' => "BiffWhistler"
        ),

        array(
        'quote' => "What do you call a dog that does magic tricks?
        A labracadabrador.",
        'author' => "leahcure"
        ),

        array(
        'quote' => "So what if I don't know what Armageddon means? It's not the end of the world.",
        'author' => "Jefferncfc"
        ),

        array(
        'quote' => "A blind man walks into a bar. And a table. And a chair.",
        'author' => "ImHully"
        ),

        array(
        'quote' => "Why did the old man fall in the well?
        Because he couldn't see that well.",
        'author' => "rangers_fan2"
        ),

        array(
        'quote' => "I bought the world's worst thesaurus yesterday. Not only is it terrible, it's terrible.",
        'author' => "Rndomguytf"
        ),

        array(
        'quote' => "This is my step ladder. I never knew my real ladder.",
        'author' => "WikiWantsYourPics"
        ),

        array(
        'quote' => "My friend asked me to help him round up his 37 sheep.
        I said \"40\"",
        'author' => "3shirts"
        ),

        array(
        'quote' => "I've found a job helping a one armed typist do capital letters.
        It's shift work.",
        'author' => "3shirts"
        ),

        array(
        'quote' => "I went bobsleighing the other day, killed 250 bobs.",
        'author' => "breadman666"
        ),

        array(
        'quote' => "I have the heart of a lion and a lifetime ban from the Toronto zoo.",
        'author' => "kailey_sara"
        ),

        array(
        'quote' => "What's the difference between a good joke and a bad joke timing.",
        'author' => "Melchiah_III"
        ),

        array(
        'quote' => "Communism jokes aren't funny unless everyone gets them.",
        'author' => "-georgie"
        ),

        array(
        'quote' => "What do the movies titanic and the sixth sense have in common.
        Icy dead people.",
        'author' => "mysevenyearitch"
        ),

        array(
        'quote' => "Two men meet on opposite sides of a river. One shouts to the other \"I need you to help me get to the other side!\"
        The other guy replies \"You are on the other side!\"",
        'author' => "The2ndKingInTheNorth"
        ),

        array(
        'quote' => "I couldn't figure out why the baseball kept getting larger. Then it hit me.",
        'author' => "KaboomBoxer"
        ),

        array(
        'quote' => "My friends say there's a gay guy in our circle of friends... I really hope it's Todd, he's cute.",
        'author' => "-917-"
        ),

        array(
        'quote' => "Guy walks into a bar and orders a fruit punch.
        Bartender says \"Pal, if you want a punch you'll have to stand in line\" Guy looks around, but there is no punch line.",
        'author' => "justacheesyguy"
        ),

        array(
        'quote' => "I've been told I'm condescending.
        (that means I talk down to people)",
        'author' => "iblinkyoublink"
        ),

        array(
        'quote' => "How did the hipster burn his mouth?
        He ate the pizza before it was cool.",
        'author' => "plax1780"
        ),

        array(
        'quote' => "Why aren’t koalas actual bears?
        They don’t meet the koalafications",
        'author' => "ImHully"
        ),

        array(
        'quote' => "It's hard to explain puns to kleptomaniacs because they always take things literally.",
        'author' => "auran98"
        ),

        array(
        'quote' => "I want to die peacefully in my sleep like my grandfather did, not screaming in terror like the passengers in his car.",
        'author' => "msdarth"
        ),

        array(
        'quote' => "2 cows are grazing in a field.
        1 cow says to the other, \"You ever worry about that mad cow disease?\".
        The other cow says, \"Why would I care? I'm a helicopter!\"",
        'author' => "Electric_Evil"
        ),
    );

    public function getQuote() {

        $quote = $this->quotes[mt_rand(0, count($this->quotes) - 1)];
        if (!isset($quote["author"])) {
            $quote["author"] = "Unknown";
        }

        $quote["quote"] = nl2br($quote["quote"]);
        return json_encode($quote);

    }
}
